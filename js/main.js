$(document).ready(function() {

    $('.close').on('click', function(event) {
        // console.log(event);
        event.preventDefault();
        $('body').removeClass('pop-up-opened');
    });

    setTimeout( function() {
        $('body').addClass('pop-up-opened');
    }, 3000 );

    $('.owl-carousel').owlCarousel({
        items: 1,
        loop: true,
        nav: true,
        navContainer: '.slider-controls'
    });


    $('.js-select2').select2({
        minimumResultsForSearch: Infinity
    });

    $('.js-calc').on("click", function(){
        console.log("Result");
    });

    $('.js-calc').on("click", function(){
        $('.js-calc-results').slideUp('slow', function() {

            var selectCurrency = $('.js-select2:not(.select-big)').val();

            calculations(selectCurrency);
        });
        
    })

    $('.js-initial-capital').keypress(function(key) {
    	if( (key.charCode < 48 || key.charCode > 57) && key.charCode != 32) return false;
    });

    $('.js-years').keypress(function(key){
        if( (key.charCode < 48 || key.charCode > 57) ) return false;
    });

    $('.js-months').keypress(function(key) {
        if( (key.charCode < 48 || key.charCode > 57) ) return false;
    });
});

function calculations(currency) {

    var currencySign;

    if(currency == 1) {
        currencySign = '&#36;'
    } else if(currency == 2) {
        currencySign = '&euro;'
    }

    $('.currency-sign').html(currencySign);

    var jsInitial = $('.js-initial-capital').val().replace(/\s+/g, '');
        
    var jsYears = $('.js-years').val();

    var jsMonths = $('.js-months').val();

    jsInitial = parseInt(jsInitial, 10);
    jsYears = parseInt(jsYears, 10);
    jsMonths = parseInt(jsMonths, 10);

    var finalMonths = (jsYears*12) + jsMonths;

    var finalBalance = jsInitial;

    var selectValue =  $('.js-select2.select-big').val();

    var percent = 2.6;

    if(selectValue == 1) {
        percent = 4.6
    } else if (selectValue == 2) {
        percent = 3.6
    } else if (selectValue == 3) {
        percent = 2.6
    };

    for(i = 1; i <= finalMonths; i++) {
        finalBalance = finalBalance*(1+percent/100);
    };

    var sum = finalBalance;
    sum = parseInt(sum, 10);
    var profit = sum - jsInitial;

    profit = profit.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
    sum = sum.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
    jsInitial = jsInitial.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
    $('.js-calc-initial').text(jsInitial);
    $('.js-calc-years').text(jsYears);
    $('.js-calc-months').text(jsMonths);

    $('.js-calc-ending-capital').text(sum);
    $('.js-calc-profit').text(profit);


    $('.js-calc-results').slideDown();
}


console.log(document);
console.log($(document)); 


// float, clearfix, css sprite, svg, difference between == ===


var header = document.getElementById('big-header');

var xhttp = new XMLHttpRequest();
xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
        header.innerHTML = this.responseText;
        // console.log(this.responseText);
    }
};
xhttp.open("GET", "./data/intro_header.txt", true);
xhttp.send();


var headerButton = document.getElementById('header-change-btn');
headerButton.onclick = function() {
    
    var ptsF = new XMLHttpRequest();
    ptsF.onreadystatechange = function() {
        if (this.readyState == 4 && this.status ==200) {
            // console.log(this.responseText);
            header.innerHTML = this.responseText;
        }
    };
    ptsF.open("GET", "./data/intro_header_updated.txt", true);
    ptsF.send();
};


var signUpButton = document.getElementById('sign-up-btn');
var signUpInput = document.getElementById('sign-up-input');

signUpButton.onclick = function() {

    var inputText = new XMLHttpRequest();
    inputText.onreadystatechange = function() {
        console.log(this);
        if (this.readyState == 4 && this.status == 200) {
            signUpInput.value = this.responseText;
        }

        
    };

    inputText.open("GET", "/data/sign_up.txt", true);
    inputText.send();
};











